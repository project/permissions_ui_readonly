permissions_ui_readonly

This module alters Permission UI page by disabling the save button.  Treats Permission Page as
an overview page for special use-cases.